const date_select = document.getElementById('date');
const submit_btn = document.querySelector('.submit_btn')

submit_btn.addEventListener('click', (e)=>{
  if(date_select.value == "") alert('Please enter a valid date')
  else{
    url = "http://127.0.0.1:5000/dow_jones";
    year = new Date(date_select.value).getFullYear();

    url += `?year=${year}`;
    fetch(url).then(pre => pre.json()).then(pre => alert(`Dow Jone's stock value at ${date_select.value} would be ${pre}`))
  }
})